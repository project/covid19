INTRODUCTION
------------

With the COVID19 pandemic we found ourselves having to deploy custom blocks to dozens of sites.
Most of them only wanted a very very simple message, but custom blocks are content in D8 and can't
be managed from configuration.

For that reason we created a very dummy module which we added to every project altering the message as
needed by our clients.

INSTALLATION
------------

 * Do **not** add as a composer dependency unless you really want the defaults.
 * Download the module and edit line 17 in src/Plugin/Block/CovidBlock.php
 * If you want edit the install file as needed.
 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

COMPOSER INSTALLATION
---------------------

Use the `sample.patch` provided in the code and patch this module with composer.

CONFIGURATION
-------------

This module **expects you to edit it**. It doesn't provide something that you can't do with Drupal core,
but it allow you to deploy things faster than going through the UI.

Stay safe!
