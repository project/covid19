<?php

namespace Drupal\covid19\Plugin\Block;

use Drupal\Core\Block\BlockBase;

define("COV_MESSAGE","Our schedule is affected by the COVID19 pandemic.<br>Events are cancelled and we are working from home.");


/**
 * Provides covid19 block.
 *
 * @Block(
 *   id = "covid19_block",
 *   admin_label = @Translation("Covid 19"),
 *   category = @Translation("Covid 19")
 * )
 */
class CovidBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [
      '#title' => $this->t('Important Information on COVID-19'),
      '#markup' => '<div class="covid-message">' . $this->t(COV_MESSAGE) . '</div>',
      '#attached' => [
        'library' => ['covid19/covid19'],
      ],
    ];
    return $build;
  }

}
